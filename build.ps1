# Set Visual Studio Variables
pushd 'C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build'
cmd /c "vcvarsall.bat x86 & set" |
foreach {
  if ($_ -match "=") {
    $v = $_.split("="); set-item -force -path "ENV:\$($v[0])"  -value "$($v[1])"
  }
}
popd

$toppath = Get-Location

git clone https://github.com/kghost/utf8proc.git
cd utf8proc
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -G "NMake Makefiles" ..
nmake
nmake install

7z a "$toppath\utf8proc.zip" utf8proc.dll utf8proc.lib utf8proc-config.cmake
cd ..
7z a "$toppath\utf8proc.zip" utf8proc.h
cd $toppath
7z a "$toppath\utf8proc.zip" utf8proc-targets.cmake

